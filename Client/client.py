import socket


HOST = '127.0.0.1'  
PORT = 12345       

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((HOST, PORT))


with open("received_file.txt", "wb") as file:
    while True:
        data = client_socket.recv(1024)
        if not data:
            break
        file.write(data)

print("File received successfully!")

client_socket.close()

with open("received_file.txt", "r") as file:
    content = file.read()
    print("Content of received_file.txt:")
    print(content)
