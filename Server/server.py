import socket

HOST = '127.0.0.1'
PORT = 12345

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen()

print("Waiting for a connection...")

client_socket, client_address = server_socket.accept()
print("Connected to:", client_address)


file_path = "a.txt"
with open(file_path, "rb") as file:
    data = file.read(1024)
    while data:
        client_socket.send(data)
        data = file.read(1024)

print("File sent successfully!")

client_socket.close()
server_socket.close()
